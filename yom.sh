#!/bin/bash

### Help ###
Help()
{
    echo "Usage: ./yom.sh [OPTIONS] [WORDS...]"
    echo
    echo "Options:"
    echo
    echo "-a NAME change author at once"
    echo "-i FILE set background image"
    echo "-h      display this help and exit"
}

### Main program ###
# Default author name.
# Change your pen-name ;)
author="みっちゃ"

# Background Image file path.
bg_image=""

# Get the options.
while getopts ":a:i:h" option; do
    case $option in
        a) # author
            author=$OPTARG;;
        
        i) # background image
            bg_image=$OPTARG;;

        h) # help
            Help
            exit;;

        \?) # Invalid option
            echo "Error: Invalid option"
            exit 1;;
    esac
done
shift $(($OPTIND-1))
          
# Make stamp. (pick up first character)
stamp=$(echo $author | awk '{print substr($0, 1, 1)}')

# Make save directory.
save_dir=~/Documents/yom/$(date "+%Y/%m/%d")
if [ ! -d $save_dir ]; then
    mkdir -p $save_dir
fi

# Replace Zenkaku spaces to Hankaku spaces.
orig=$(echo $@ | sed -e 's/　/ /g')

# Change the end of sentence to <br> tag element.
yom=()
for p in ${orig[@]}; do
    yom+=$(echo ${p} | sed -e 's/$/<br>/g')
done

# Count html files at save directory.
file_count=$(find $save_dir -name "*.html" -type f | wc -l)

# Create html file name.
file_name=$((file_count+1)).html

# Default color.
bg_color="#fffff3"
font_color="#323232"
date_color="#868686"

# Copy image file to save directory.
image_file_name=$((file_count+1)).${bg_image##*.}
if [[ ! -z $bg_image ]]; then
    cp "$bg_image" $save_dir/$image_file_name

    # Change color.
    bg_color="rgba(0, 0, 0, 0.4)"
    font_color="#fefefe"
    date_color="#cdcdcd"
fi

# Generate html code and insert file.
cat > $save_dir/$file_name<<EOF
<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>「${orig[@]}」 $author</title>
    <style>
      html, body, .container {
        margin: 0;
        padding: 0;
        height: 100%;
      }
      body {
        background-color: #fffff3;
        color: $font_color;
        background-image: url($image_file_name);
        background-position: center center;
        background-repeat: no-repeat;
        background-size: 128%;
      }
      .container {
        background-color: $bg_color;
      }
      .main {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: row-reverse;
        height: 93%;
      }
      h1, h2 {
        margin: 0;
        line-height: 2em;
        font-family: serif;
        font-weight: normal;
        letter-spacing: 0.5em;
        writing-mode: vertical-rl;
      }
      h2 {
        padding-top: 3em;
        letter-spacing: 0.3em;
      }
      h2 span {
        padding: 0;
        border: 3px solid #d94236;
        border-radius: 3px;
        color: #d94236;
        font-weight: bold;
        font-family: sans-serif;
        font-size: 114%;
        letter-spacing: 0;
      }
      .date {
        padding: 1em;
        font-size: 80%;
        color: $date_color;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="main">
        <h1>$yom</h1>
        <h2>$author<span>$stamp</span></h2>
      </div>
      <div class="date">$(date '+%Y/%m/%d(%a) %H:%M')</div>
    </div>
  </body>
</html>
EOF

# Open web browser.
xdg-open $save_dir/$file_name &
