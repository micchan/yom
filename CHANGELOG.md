# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Calendar Versioning](https://calver.org/).

## Unreleased

- Nothing..

## Version [20230108.101](https://gitlab.com/micchan/yom/-/tags/v20230108.101)

### Removed

- Remove unused valiable.

## Version [20221221.1](https://gitlab.com/micchan/yom/-/tags/v20221221.1)

- First release.
